Categories:System
License:Apache2
Web Site:http://forum.xda-developers.com/android/apps-games/app-kernel-adiutor-t2986129
Source Code:https://github.com/Grarak/KernelAdiutor
Issue Tracker:https://github.com/Grarak/KernelAdiutor/issues

Auto Name:Kernel Adiutor
Summary:Manage kernel parameters
Description:
Tweak and monitor kernel parameters. Depending on the device and kernel
used, this includes:

* CPU (Frequency, Governor, Voltages)
* GPU (Frequency, Governor)
* Screen (Color Calibration [RGB])
* Wake (DT2W, S2W)
* Sound (Faux Sound)
* Battery (Fast Charge)
* I/O Scheduler
* Kernel Samepage Merging
* Low Memory Killer (Minfree settings)
* Virtual Machine
* Build prop Editor
* etc.

Features which aren't supported by your device won't show up in the first place.
.

Requires Root:Yes

Repo Type:git
Repo:https://github.com/Grarak/KernelAdiutor

Build:0.6.2,31
    commit=1f6c14888ba8a551c7321d02d30d194fb03e9368
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.6.3,32
    commit=f5484ba084bc046b5632ce1dbfe21477ba2e292a
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.6.4,33
    commit=47752b4a323298b2f4df9608520b1a97d51b26ac
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.7.1,35
    commit=d353c935b2e684ea0f472634e597be823234baf6
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.8,37
    commit=d055d5e6791c3bc11adbd628fb3e64a1bebc9dc0
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.8.1,38
    commit=d07b366586c6addacb1c98e6683540eeda16f345
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.8.1.1 beta
Current Version Code:39

